# Gideon's Analysis
This repository houses the analysis for Gideon's research.

# Setup
To be able to use this code, you must install the [ROOT](https://root.cern/) software package. It has been 
found to work with `v6-18-02` and higher.

## Docker
This can also be run within a [docker](https://www.docker.com/) container, which is built [here](https://gitlab.com/smeehan12/root-builds).  You
will have to learn how to use docker, of course, but after doing that and installing it 
on your machine, then you can run the following command from within this cloned repository
```
docker run --rm -it -w $PWD -v $PWD:$PWD registry.gitlab.com/meehan/root-build
```
this will put you into the container instance and in the same directory where you currently 
reside, that is, in this repository.  You can then set up root as follows :
```
source /code/build/bin/thisroot.sh
```

# Running
You first need to build the code
```
g++ -o analysis.exe analysis.cpp `root-config --cflags --glibs`
```
which will produce the executable `analysis.exe`.  This can then
be run with the example data file stored in [/data]() in this repository.  
This command requires three arguments in the order shown here
```
./analysis.exe <INPUTDATA_FILE_1> <INPUTDATA_FILE_1> <OUTPUT_FILE>
```
these arguments must be provided in the specific order shown : 
  - `<INPUTDATA_FILE_1>` : This is one of the input data files in `.dat` format
  - `<INPUTDATA_FILE_1>` : This is the second input data files in `.dat` format
  - `<OUTPUT_FILE>` : This is an arbitrary name of the output `.root` format.  Note that this name must end in `.root`
So an example run command would be the following if you rely on the example data
provided in the repository.
```
./analysis.exe data/filling-listmode-29-06-2017-Diamond_Double_Doppler2_ch000.dat \
               data/filling-listmode-29-06-2017-Diamond_Double_Doppler2_ch000.dat \
               outputDataFile.root
```
which will produce the root file and also the output plot `outputPlot.pdf`.