#include <TH2D.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TMath.h>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <TFile.h>
#include <TNtuple.h>
#include <TSystem.h>
using namespace std;

TObject *MyObj = 0;

// structure for tree events
struct event {
  long long t_1;
  long long t_2;
  long e_1;
  long e_2;
  Bool_t coinc;
};


int main(int argc, char **argv) {

  if(argc==1){
    printf("Usage  : %s  <inputfile 1>  <inputfile 2>  <rootfile> \n",argv[0]);
    printf("Option : nothing yet \n");
    exit(0);
  }

  // Extract input file name 1 from the argument list
  char input_file_1[100];   
  strcpy(input_file_1,argv[1]);

  // Extract input file name 2 from the argument list
  char input_file_2[100];   
  strcpy(input_file_2,argv[2]);

  std::cout<<"Running on input files :"<<std::endl;
  std::cout<<"InputFile 1 : "<<input_file_1<<std::endl;
  std::cout<<"InputFile 2 : "<<input_file_2<<std::endl;


  // Extract ROOT output file name from the argument list
  char root_file[64];   // root file
  strcpy(root_file,argv[3]);

  std::cout<<"Output ROOT file :"<<std::endl;
  std::cout<<"OutputFile : "<<root_file<<std::endl;

  // The input .dat file
  TString input_1 = input_file_1;
  TString input_2 = input_file_2;

  ifstream file_1;
  ifstream file_2;

  file_1.open (input_1);
  file_2.open (input_2);

  // event tree container
  event data_event;

  // The output .root file
  TString filename_ROOT = root_file;
  TFile *f = new TFile(filename_ROOT,"RECREATE");

  // for the energy histos per iteration and perspex pellet
  // From the python coincidence finder programme
  // energy1 = 0.7363*long(channel1)+0.8373
  // energy2 = 0.5424*long(channel2)+0.1738
  // inverting
  // channel1 = (energy1 - 0.8373)/0.7363
  // so 511 is at 692.87 ~ 693
  // channel2 = (energy2 - 0.1738)/0.5424
  // so 511 is at 941.79 ~ 941


  // analysis histograms
  TH1D *spec_1 = new TH1D("spec_1", "spec_1", 4096, -0.5, 4095.5);
  TH1D *spec_2 = new TH1D("spec_2", "spec_2", 4096, -0.5, 4095.5);
  TH2D *spec_C = new TH2D("spec_C", "spec_C", 4096, -0.5, 4095.5, 4096, -0.5, 4095.5);
  TH2D *spec_C511 = new TH2D("spec_C1", "spec_C1", 41, 693-20, 693+20, 41, 940-20, 940+20);

  TTree *data = new TTree("data","data");
  data->Branch("event",&data_event.t_1,"t_1/I:t_2/I:e_1/I:e_2/I:coinc/O");

  std::cout<<"Starting data processing with : "<<std::endl;
  std::cout<<input_1<<std::endl;
  std::cout<<input_2<<std::endl;

  // Maximum time difference for channels to be in coincidence
  float Maxtdiff = 100;

  // Track line numbers in files
  long linenum1 = 0;
  long linenum2 = 0;
  long energy_1, energy_2;
  long long time_1, time_2;
  std::string::size_type sz_1 = 0, sz_2 = 0;   // alias of size_t

  // Read past headers
  std::string str_1;
  std::string str_2;
  while( file_1.good() && file_2.good() ){
    std::getline(file_1, str_1);
    std::getline(file_2, str_2);
    if(str_1[0] != 'H') break ; //for comments
  }

  // for unpacking the data files
  time_1 = std::stoll(str_1,&sz_1,0);
  time_2 = std::stoll(str_2,&sz_2,0);
  str_1 = str_1.substr(sz_1);
  str_2 = str_2.substr(sz_2);
  energy_1 = std::stol(str_1,&sz_1);
  energy_2 = std::stol(str_2,&sz_2);
  data_event.coinc = false;


  std::cout<<"Reading in data  ..... "<<std::endl;


  // Loop over events and match times
  while(file_1.good() && file_2.good() ){
    // If the time difference is greater than specified loop until two values within the window are found
    if (abs(time_1-time_2)>Maxtdiff){
      // Read in new data from which ever file has the lower time value
      if (time_1 < time_2){
        data_event.t_1=time_1; data_event.t_2=0; data_event.e_1 = energy_1 ; data_event.e_2 = 0;
        data->Fill();
        spec_1->Fill(energy_1);
        std::getline(file_1, str_1);
        linenum1 += 1;
        if (str_1.length() > 1) {
          time_1 = std::stoll(str_1,&sz_1,0);
          str_1 = str_1.substr(sz_1);
          energy_1 = std::stol(str_1,&sz_1);
        }
      }

      if (time_2 < time_1){
        data_event.t_1=0; data_event.t_2=time_2; data_event.e_1 = 0 ; data_event.e_2 = energy_2;
        data->Fill();
        spec_2->Fill(energy_2);
        std::getline(file_2, str_2);
        linenum2 += 1;
        if (str_2.length() > 1) {
          time_2 = std::stoll(str_2,&sz_2,0);
          str_2 = str_2.substr(sz_2);
          energy_2 = std::stol(str_2,&sz_2);
        }
      }
    }

    if (abs(time_1-time_2)<=Maxtdiff) {
      data_event.t_1=time_1; data_event.t_2=time_2; data_event.e_1 = energy_1 ; data_event.e_2 = energy_2;
      data_event.coinc = true;
      data->Fill();   //Wite the coincidence event
      spec_1->Fill(energy_1);
      spec_2->Fill(energy_2);
      spec_C->Fill(energy_1,energy_2);
      if (energy_1 > (693-20) && energy_1 < (693+20) && energy_2 > (940-20) && energy_2 < (940+20)) {
        spec_C511->Fill(energy_1,energy_2);
      }
      data_event.coinc = false;
      std::getline(file_1, str_1);
      std::getline(file_2, str_2);
      linenum1+= 1;  linenum2 += 1;
      if (str_1.length() > 1 && str_2.length() > 1) {
        time_1 = std::stoll(str_1,&sz_1,0);
        time_2 = std::stoll(str_2,&sz_2,0);
        str_1 = str_1.substr(sz_1);
        str_2 = str_2.substr(sz_2);
        energy_1 = std::stol(str_1,&sz_1);
        energy_2 = std::stol(str_2,&sz_2);
      }
    }
  }

  // draw the analysis validation plots
  TCanvas *MyCanvas = new TCanvas("canv", "Stuff",1200,800);
  MyCanvas->Divide(2,2);

  MyCanvas->cd(1);
  spec_1->SetBins(4096, 0.47, 3016.35);
  spec_1->Draw();
  MyCanvas->cd(2);
  spec_2->SetBins(4096, -0.10, 2221.57);
  spec_2->Draw();
  MyCanvas->cd(3);
  spec_C->SetBins(4096, 0.47, 3016.35, 4096, -0.10, 2221.57);
  spec_C->Draw();
  MyCanvas->cd(4);
  spec_C511->SetBins(41, 0.7363*673+0.8373, 0.7363*713+0.8373, 41, 0.5424*921+0.1738, 0.5424*961+0.1738);
  gPad->SetLogz();
  spec_C511->Draw("colz");
  MyCanvas->Update();
  
  MyCanvas->SaveAs("outputPlot.pdf");

  // Close the input and output files
  f->Write();
  file_1.close();
  file_2.close();

  cout << "Finished reading "<<linenum1<<" lines from "<< input_1<<" and " <<linenum2<<" lines from "<< input_2<<" \n";

}